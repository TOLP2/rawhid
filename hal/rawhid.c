/********************************************************************
* Description:  hal_rawhid.c
*			   Driver for an Arduino configured a Raw HID, with 
*			   configurable encoders, buttons and outputs.
*
* Author: Peter van Tol
* License: GPL Version 2
* Copyright (c) 2021.
*
********************************************************************/

// LinuxCNC includes
#include "rtapi.h"		/* RTAPI realtime OS API */
#ifdef RTAPI
#include "rtapi_app.h"
#endif
#include "rtapi_bitops.h"
#include "rtapi_errno.h"
#include "rtapi_math64.h"
#include "rtapi_string.h"
#include "hal.h"

// Regular includes
#include <linux/input.h>
#include <linux/hidraw.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdlib.h> 
#include <stdint.h>   /* Standard types */
#include <string.h>   /* String function definitions */
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>	/* File control definitions */
#include <errno.h>	/* Error number definitions */


#ifdef MODULE_INFO
MODULE_INFO(linuxcnc, "component:rawhid:Services an Arduino based Raw HID device with encoders, buttons, and outputs");
MODULE_INFO(linuxcnc, "pin:encoder_##_count:s32:0:out:Signal containing the current count of the encoder:0:None");
MODULE_INFO(linuxcnc, "author:Peter van Tol petertgvantolATgmailDOTcom");
MODULE_INFO(linuxcnc, "license:GPL");
MODULE_LICENSE("GPL");
#endif // MODULE_INFO


/***********************************************************************
*				STRUCTURES AND GLOBAL VARIABLES					   *
************************************************************************/
#define PKT_REQUESTDATA 0x01
#define PKT_ENCODER_COUNTS 0x60
#define PKT_ENDOFCOMMAND 0xff

/* These define the maximum of the buttons and encoders */
#define MAX_ENCODERS     4
#define MAX_BUTTONS     32
#define MAX_DIGITAL_IN  32

/* Signal request of data  */
#pragma pack(push,1)
typedef enum {DATA_REQ_NONE, DATA_REQ_VERSION, DATA_REQ_CAPABILITIES, DATA_REQ_REPORT} dataRequested;
typedef struct {
  uint8_t len;	  // packet length in bytes, including this one = 3
  uint8_t id;	   // id=PKT_REQUESTDATA
  dataRequested data;
} sPkt_RequestData;
#pragma pack(pop)

/* Signal response of the version */
#define PKT_VERSION 0x02
/*#pragma pack(push,1)
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 2
  uint8_t id;       // id=PKT_VERSION
  char string[sizeof(VERSION_STRING)];
} sPkt_Version;
#pragma pack(pop)*/

/* Signal response of the capabilities */
#define PKT_CAPABILITIES 0x03
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 2
  uint8_t id;         // id=PKT_CAPABILITIES
  uint8_t encoders;   // Number of encoders
  uint8_t digital_in; // Number of digital inputs
  uint8_t buttons;    // Number of buttons (digital inputs with debounce and click detection)
} sPkt_Capabilities;
#pragma pack(pop)


#define PKT_DIGITAL_IN 0x70
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 22 bytes
  uint8_t id;         // id=PKT_DIGITAL_IN_STATE
  uint32_t active;
} sPkt_RawHid_digital_in_v1;
#pragma pack(pop)

#define PKT_BUTTON_STATES 0x80
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 22 bytes
  uint8_t id;         // id=PKT_BUTTON_STATES
  int32_t pushed;        
  int32_t released;         
  int32_t longPressed;     
  int32_t clicked;       
  int32_t doubleClicked;
} sPkt_RawHid_buttons_v1;
#pragma pack(pop)


/* Signal end of command packets. Note that no filler bytes are needed  */
#pragma pack(push,1)
typedef struct {
  uint8_t len;	  // packet length in bytes, including this one = 2
  uint8_t id;	   // id=PKT_ENDOFCOMMAND
} sPkt_Endofcommand;
#pragma pack(pop)


#pragma pack(push,1)
typedef struct {
	sPkt_RequestData request;
	sPkt_Endofcommand eoc;
} h2c_requestframe_t;
static h2c_requestframe_t h2c_frame;
#pragma pack(pop)



typedef struct {
	unsigned short vid; /* The vendor id of the device, e.g. 0x2341 for Arduino SA*/ 
	unsigned short pid; /* The product id of the device, e.g. 0x8036 for a Leonardo*/
} usb_identifier_t;

typedef struct {
	uint8_t encoders;	/* The actual number of encoders on the device. Must be < MAX_ENCODERS*/
	uint8_t digital_in;
	uint8_t buttons;	/* The actual number of buttons on the device. Must be < MAX_BUTTONS*/
} device_capabilities_t;

typedef struct {
	hal_bit_t *pushed;
	hal_bit_t *released;	
	hal_bit_t *clicked;	
	hal_bit_t *doubleClicked;	
	hal_bit_t *longPress;
} hal_button_data_t;

typedef struct {
	hal_u32_t *raw;
	hal_bit_t *in[MAX_DIGITAL_IN];
	hal_bit_t *in_not[MAX_DIGITAL_IN];
} hal_digital_in_data_t;



/* This structure contains the runtime data needed by the RawHID driver 
for a single connected device.
*/
typedef struct {
	usb_identifier_t usbid;
	int fd;								/* The file-descriptor for writing and reading data*/
	char firmware[64];					/* Contains the version string of the firmware */
	device_capabilities_t capabilities;	/* Struct containing the capabilities of the device (number of encoders, inputs, etc.)*/ 
	uint8_t xmitbuf[65];				/* The buffer for transmitting and receiving data from the device */
	h2c_requestframe_t h2c_frame;		/* The frame for requesting data from the device */
	
	// Data as received from the device
	hal_s32_t *encoders[MAX_ENCODERS];
	hal_button_data_t buttons[MAX_BUTTONS];
	hal_digital_in_data_t digital_in;
 	
} hidraw_t;

/* Pointer to array of hidraw_t structs in shared memory, 1 per device */
static hidraw_t *hidraw_devices;
	
/* Other globals */
static int comp_id;		/* component ID */
static int num_devices;	/* number of devices configured */


/***********************************************************************
*				  LOCAL FUNCTION DECLARATIONS						 *
************************************************************************/
/* 
 * Definition of the init and exit code
 */
int __comp_parse_count(int *argc, char **argv);
int __comp_parse_names(int *argc, char **argv);
int __comp_parse_config(int *argc, char **argv); 
int main(int argc_, char **argv_);
int rtapi_app_main(void);
void rtapi_app_exit(void);

/* 
 * 'pins_and_params()' does most of the work involved in setting up
 * the driver.  It parses the command line (argv[]), then if the
 * command line is OK, it calls hal_init(), allocates shared memory
 * for the hidraw_t data structure(s), and exports pins and parameters.
 */
static int pins_and_params(char *name, hidraw_t *device);


/*
 * Functions for communicating with the USB devices
 */ 
static int setup_device(hidraw_t *device);
static int init_usb_device(hidraw_t *device);
static int request_version(hidraw_t *device);
static int request_capabilities(hidraw_t *device);
void write_to_device(hidraw_t *device);
void read_and_parse(hidraw_t *device);


static void user_mainloop(void);

/***********************************************************************
*					   INIT AND EXIT CODE						   	   *
***********************************************************************/
#define MAX_DEVICES 8
static int default_count=1, count=0;
char *names[MAX_DEVICES] = {0,};
int argc=0; char **argv=0;

/**
 * Main entrypoint for the component. Reads and parses the arguments and starts the loop
 */ 
int main(int argc_, char **argv_) {
	// Copy the arguments, as this function will modify these
	argc = argc_; argv = argv_;
	
	// For developing purpose, print the arguments
	rtapi_print("Arguments (%X):\n", argc);
	int i;
	for (i = 0; i < argc; i++)
		rtapi_print("   %s\n", argv[i]);
	
	// Parse the count and names 
	int found_count, found_names, found_config;
	found_count = __comp_parse_count(&argc, argv);
	found_names = __comp_parse_names(&argc, argv);
	if (found_count && found_names) {
		rtapi_print_msg(RTAPI_MSG_ERR, "count= and names= are mutually exclusive\n");
		return 1;
	}
	// If count is not set, use the default (1)
	if(!count && !found_names) count = default_count; 
	// If names is not set, copy the numbers to the names list
	if (found_count) {
		for (i = 0; i < count; i++) {
			char* buf = malloc(16 * sizeof(char));
			rtapi_snprintf(buf, sizeof(buf), "%d", i);
			names[i] = buf;
		}
	}
	// Calculate the number of devices and size the array accordingly
	num_devices = 0;
	for(i=0; (i < MAX_DEVICES) && names[i]; i++) {
		num_devices++;
	}
	
	if(rtapi_app_main() < 0) return 1;
	user_mainloop();
	rtapi_app_exit();
	return 0;
}


int rtapi_app_main(void) {
	
	// Variable to store the state
	int i, r = 0;
	
	// Initialize the component. On error (comp_id < 0), pass the error onward
	comp_id = hal_init("rawhid");

	hidraw_devices = hal_malloc(num_devices * sizeof(hidraw_t));
	
	// Load the configuration for each device and start connection
	if (__comp_parse_config(&argc, argv) < 0) return 1;
	for (i = 0; i < num_devices; i++) {
		if (setup_device(&hidraw_devices[i]) < 0) {
			rtapi_print("HIDRAW: ERROR: Failed to setup device(s).\n");
			return 1;
		}
	}
	
	// Connect with the pins (NOT IMPLEMENTED YET)
	for (i = 0; i < num_devices; i++) {
		r += pins_and_params(names[i], &hidraw_devices[i]);
	}
	
	// If an error occurred, exit the plugin, otherwise report ready for action
	if(r) rtapi_app_exit;
	else hal_ready(comp_id);
	
	// Return the state
	return r;
}


/**
 * This function is called when the application is closed. It makes sure the
 * connection to the USB is properly closed.
 */
void rtapi_app_exit(void) {
	// Close the connection to the USB device, if one is open
	int i;
	for (i = 0; i < num_devices; i++) {
		if (hidraw_devices[i].fd > 0) close (hidraw_devices[i].fd);
	}
	// Close this component
	hal_exit(comp_id);
}


/**
 * This function is the workhorse which sets all the pins and params for each
 * device
 */
static int pins_and_params(char *name, hidraw_t *device){
	
	int i, ret = 0;
	
	/* Pins for the encoders */
	for (i = 0; i < device->capabilities.encoders; i++) { 
		ret += hal_pin_s32_newf(HAL_OUT, &(device->encoders[i]), comp_id,
            "rawhid.%s.encoder-%02d-count", name, i);
	}
	
	/* Pins for digital in */
	if (device->capabilities.digital_in > 0) {
		ret += hal_pin_u32_newf(HAL_OUT, &(device->digital_in.raw), comp_id,
            "rawhid.%s.digital-in-raw", name, i);
	}
	for (i = 0; i < device->capabilities.digital_in; i++) { 
		ret += hal_pin_bit_newf(HAL_OUT, &(device->digital_in.in[i]), comp_id,
            "rawhid.%s.digital-%02d-in", name, i);
		ret += hal_pin_bit_newf(HAL_OUT, &(device->digital_in.in_not[i]), comp_id,
            "rawhid.%s.digital-%02d-in-not", name, i);
	}
	
	/* Pins for the input buttons */
	for (i = 0; i < device->capabilities.buttons; i++) { 
		ret += hal_pin_bit_newf(HAL_IO, &(device->buttons[i].pushed), comp_id,
            "rawhid.%s.button-%02d-pushed", name, i);
		ret += hal_pin_bit_newf(HAL_IO, &(device->buttons[i].released), comp_id,
            "rawhid.%s.button-%02d-released", name, i);
		ret += hal_pin_bit_newf(HAL_IO, &(device->buttons[i].clicked), comp_id,
            "rawhid.%s.button-%02d-clicked", name, i);
		ret += hal_pin_bit_newf(HAL_IO, &(device->buttons[i].doubleClicked), comp_id,
            "rawhid.%s.button-%02d-double-click", name, i);
		ret += hal_pin_bit_newf(HAL_IO, &(device->buttons[i].longPress), comp_id,
            "rawhid.%s.button-%02d-long-press", name, i);
	}
	
	return ret;
	
}

/***********************************************************************
*						HELPER FUNCTIONS							  *
***********************************************************************/

/*
 * Function which checks for the count 
 */
int __comp_parse_count(int *argc, char **argv) {
	int i;
	for (i = 0; i < *argc; i ++) {
		if (strncmp(argv[i], "count=", 6) == 0) {
			errno = 0;
			count = strtoul(&argv[i][6], NULL, 0);
			for (; i+1 < *argc; i ++) {
				argv[i] = argv[i+1];
			}
			argv[i] = NULL;
			(*argc)--;
			if (errno == 0) {
				return 1;
			}
		}
	}
	return 0;
}

/*
 * Function which checks and parses the names 
 */
int __comp_parse_names(int *argc, char **argv) {
	int i;
	for (i = 0; i < *argc; i ++) {
		if (strncmp(argv[i], "names=", 6) == 0) {
			char *p = &argv[i][6];
			int j;
			for (; i+1 < *argc; i ++) {
				argv[i] = argv[i+1];
			}
			argv[i] = NULL;
			(*argc)--;
			for (j = 0; j < 16; j ++) {
				names[j] = strtok(p, ",");
				p = NULL;
				if (names[j] == NULL) {
					return 1;
				}
			}
			return 1;
		}
	}
	return 0;
}

/*
 * Function which checks and parses the config 
 */
int __comp_parse_config(int *argc, char **argv) {
	int i, j, k;
	for (i = 0; i < *argc; i ++) {
		if (strncmp(argv[i], "cfg=", 4) == 0) {
			// Get the part of the string which contains the data
			char *p = &argv[i][4];
			
			// Consume the argument
			for (; i+1 < *argc; i ++) {
				argv[i] = argv[i+1];
			}
			argv[i] = NULL;
			(*argc)--;
			
			// Skip any leading "
			while( *p == '"' ) {
				p++;
			}
			
			// Parse the config string (eg. "0x2341:0x8036 0x2341:0x8037"
			char *configs[num_devices];
			for (j = 0; j < num_devices; j++) {
				// Split the string
				configs[j] = strtok(p, " ");
				p = NULL;
				rtapi_print("config: %s\n", configs[j]);
				if (configs[j] == NULL) {
					return -1;
				}
			}
			for (j = 0; j < num_devices; j++) {		
				// Split the string again,
				char *vid = strtok(configs[j], ":");
				configs[j] = NULL;
				char *pid = strtok(configs[j], ":");
				if (pid == NULL) {
					return -1;
				}
				// the string in the device
				hidraw_devices[j].usbid.vid = strtol(vid, NULL, 0);
				hidraw_devices[j].usbid.pid = strtol(pid, NULL, 0);
			}
		}
	}
	return 0;
}


/***********************************************************************
*				 USB SETUP AND COMMUNICATION					   	   *
***********************************************************************/

/**
 * Function which performs the setup of the device
 */
static int setup_device(hidraw_t *device) {
	int ret; 

	// Connect to the device
	ret = init_usb_device(device);
	if (ret < 0) return -1;
	
	// Initialize data frame which is used to transfer data from the host
	// to the machine
	device->h2c_frame.request.id = PKT_REQUESTDATA;
	device->h2c_frame.request.len = sizeof(sPkt_RequestData);
	device->h2c_frame.eoc.id = PKT_ENDOFCOMMAND;
	device->h2c_frame.eoc.len = sizeof (sPkt_Endofcommand);
	
	// Request the version and capabilities from the device
	if (request_version(device) < 0) return -1;
	if (request_capabilities(device) < 0) return -1;
	
	// Succesfully initialised the USB device
	return 0;
}



/**
 * This function opens an USB device with the given PID and VID. Returns -1
 * when no device can be found with these parameters. The error will also be
 * printed to the command-line
 */
static int init_usb_device(hidraw_t *device) {
	char buf[16];
	int i, res;
	struct hidraw_devinfo info;
	// Brute-force open a couple of /dev/hidrawX devices 
	for (i=0;i<16;i++) {
		rtapi_snprintf(buf, sizeof(buf), "/dev/hidraw%d", i);
		device->fd = open(buf, O_RDWR|O_NONBLOCK);
		if (device->fd) {
			// Opening the device succeeded. Extract VID/PID and compare
			memset(&info, 0x0, sizeof(info));
			res = ioctl(device->fd, HIDIOCGRAWINFO, &info);
			if (res >= 0) {
				if ((info.vendor & 0xFFFF) == device->usbid.vid && (info.product & 0xFFFF ) == device->usbid.pid) {
					// Found the requested device. Return the usbfd. */
					rtapi_print("RAWHID: Found device VID:%X/PID:%X at %s\n", device->usbid.vid, device->usbid.pid,buf);
					return 0;
				}
			}
			// Not the correct device or we could not extract VID/PID. Close usbfd and
			// proceed to the next device
			close (device->fd);
		}
	}
	rtapi_print_msg(RTAPI_MSG_ERR, "RAWHID: ERROR: no device with VID:%X/PID:%X found!", device->usbid.vid, device->usbid.pid);
	device->fd = -1;
	return -1;
}


/**
 * Function which requests the version from the device
 */
static int request_version(hidraw_t *device) {
	
	// Set the request data to VERSION
	device->h2c_frame.request.data = DATA_REQ_VERSION;
	
	// Send the request to the controller
	write_to_device(device);

	// An old package might be still in the buffer of the device. This function will read
	// several times until a version string is received. Normally only 1 (one) package is
	// in the buffer, so the parsing is done for maximum four times.
	int package_counter = 0;
	while (1) {
		// The port is opened non-blocking, so we have to wait
		int ret = 0;
		int read_counter = 0;
		while (ret < 64) {
			ret = read(device->fd, device->xmitbuf, 64);
			read_counter++;
			if (read_counter > 100) {
				rtapi_print("Error no response: bytes received %d \n", ret);
				rtapi_print_msg(RTAPI_MSG_ERR, "RAWHID: ERROR: No response from device VID:%X/PID:%X.", device->usbid.vid, device->usbid.pid);
				return -1;
			}
			usleep(10000);
		}
		
		// Package received, increment counter
		package_counter++;
		
		// Get the response from the controller
		uint8_t *data;
		uint8_t idx = 0;
		while (idx < 64) {
			uint8_t idx = 0;
			data = &device->xmitbuf[idx];
			if (data[0]) {
				//rtapi_print("Parsing data %X (length %d)\n", data[1], data[0]);
				switch (data[1]) {
					/* Encoder data? */
					case PKT_VERSION:
						strcpy(device->firmware, device->xmitbuf + 2);
						rtapi_print("RAWHID: Version data recieved: %s\n", device->firmware);
						return 0;
					/* End of data frame command */
				case PKT_ENDOFCOMMAND:
					idx = 64;
					break;
				}
				// Go to next packet (or next byte if there was none)
				idx += data[0];
			} else {
				idx++;
			}
		}
		
		if (package_counter > 4) {
			rtapi_print_msg(RTAPI_MSG_ERR, "RAWHID: ERROR: Could not fetch the version of device VID:%X/PID:%X.", device->usbid.vid, device->usbid.pid);
			return -1;
		}
	}
}

/**
 * Function which capabilities the version from the device
 */
static int request_capabilities(hidraw_t *device) {
	
	// Set the request data to VERSION
	device->h2c_frame.request.data = DATA_REQ_CAPABILITIES;
	
	// Send the request to the controller
	write_to_device(device);

	// An old package might be still in the buffer of the device. This function will read
	// several times until a version string is received. Normally only 1 (one) package is
	// in the buffer, so the parsing is done for maximum four times.
	int package_counter = 0;
	while (1) {
		// The port is opened non-blocking, so we have to wait
		int ret = 0;
		int read_counter = 0;
		while (ret < 64) {
			ret = read(device->fd, device->xmitbuf, 64);
			read_counter++;
			if (read_counter > 100) {
				rtapi_print_msg(RTAPI_MSG_ERR, "RAWHID: ERROR: No response from device VID:%X/PID:%X.", device->usbid.vid, device->usbid.pid);
				return -1;
			}
			usleep(10000);
		}
		
		// Package received, increment counter
		package_counter++;
		
		// Get the response from the controller
		uint8_t idx = 0;
		uint8_t *data;
		while (idx < 64) {
			uint8_t idx = 0;
			data = &device->xmitbuf[idx];
			if (data[0]) {
				//rtapi_print("Parsing data %X (length %d)\n", data[1], data[0]);
				switch (data[1]) {
					/* Encoder data? */
					case PKT_CAPABILITIES:
						device->capabilities.encoders   = ((sPkt_Capabilities *)data)->encoders;
						device->capabilities.digital_in = ((sPkt_Capabilities *)data)->digital_in;
						device->capabilities.buttons    = ((sPkt_Capabilities *)data)->buttons;
						rtapi_print("RAWHID: Capabilities:\n");
						rtapi_print("RAWHID:   ENCODERS:    %d\n", device->capabilities.encoders);
						rtapi_print("RAWHID:   DIGITIAL_IN: %d\n", device->capabilities.digital_in);
						rtapi_print("RAWHID:   BUTTONS:     %d\n", device->capabilities.buttons);
						return 0;
					/* End of data frame command */
				case PKT_ENDOFCOMMAND:
					idx = 64;
					break;
				}
				// Go to next packet (or next byte if there was none)
				idx += data[0];
			} else {
				idx++;
			}
		}
		
		if (package_counter > 4) {
			rtapi_print_msg(RTAPI_MSG_ERR, "RAWHID: ERROR: Could not fetch the capabilities of device VID:%X/PID:%X.", device->usbid.vid, device->usbid.pid);
			return -1;
		}
	}
}
	
	
/**
 * This function opens an USB device with the given PID and VID. Returns -1
 * when no device can be found with these parameters. The error will also be
 * printed to the command-line
 */
void write_to_device(hidraw_t *device) {
	
	// Prepare transmit buffer
	device->xmitbuf[0] = 0;
	uint8_t *xmitptr = device->xmitbuf + 1;
	memcpy (xmitptr, &device->h2c_frame, sizeof(h2c_requestframe_t)); xmitptr+=sizeof(h2c_requestframe_t);
	
	// Transmit frame to USB device 
	int ret = write(device->fd, device->xmitbuf, 65);
	if (ret != 65) {
		rtapi_print_msg(RTAPI_MSG_ERR, "RAWHID: USB transmit failed: %d bytes transmitted\n", ret);
	}	
	
}

/**
 * This functions reads from the USB-device (non-blocking) and parses the received data
 * to the relevant fields of the device.
 */
void read_and_parse(hidraw_t *device) {
	// Read data from the device and check whether data has been received
	int i;
	uint8_t *data;
	if (read(device->fd, device->xmitbuf, 64) == 64) {
		// Parse the data
		uint8_t idx = 0;
		while (idx < 64) {
			data = &device->xmitbuf[idx];
			if (data[0]) {
				//rtapi_print("Parsing data %X (length %d)\n", data[1], data[0]);
				switch (data[1]) {
					/* Encoder data? */
					case PKT_ENCODER_COUNTS:
						for (i=0;i < device->capabilities.encoders; i++) {
							*(device->encoders[i]) = data[2 + i*4] | data[3 + i*4] << 8 | data[4 + i*4] << 16 | data[5 + i*4] << 24;
						}
						break;
					case PKT_DIGITAL_IN:
						*(device->digital_in.raw) = ((sPkt_RawHid_digital_in_v1 *)data)->active;
						for (i=0;i < device->capabilities.digital_in; i++) {
							if (((sPkt_RawHid_digital_in_v1 *)data)->active & (1<<i)) {
								*(device->digital_in.in[i]) = 1;
								*(device->digital_in.in_not[i]) = 0;
							} else {
								*(device->digital_in.in[i]) = 0;
								*(device->digital_in.in_not[i]) = 1;
							}
						}
					case PKT_BUTTON_STATES:
						for (i=0;i < device->capabilities.buttons; i++) {
							if (((sPkt_RawHid_buttons_v1 *)data)->pushed & (1<<i))
								*(device->buttons[i].pushed) = 1;
							if (((sPkt_RawHid_buttons_v1 *)data)->released & (1<<i)) 
								*(device->buttons[i].released) = 1;
							if (((sPkt_RawHid_buttons_v1 *)data)->clicked & (1<<i))
								*(device->buttons[i].clicked) = 1;
							if (((sPkt_RawHid_buttons_v1 *)data)->doubleClicked & (1<<i))
								*(device->buttons[i].doubleClicked) = 1;
							if (((sPkt_RawHid_buttons_v1 *)data)->longPressed & (1<<i))  
								*(device->buttons[i].longPress) = 1;					
						}
						break;
					/* End of data frame command */
					case PKT_ENDOFCOMMAND:
						idx = 64;
						break;
				}
				// Go to next packet (or next byte if there was none)
				idx += data[0];
			} else {
				idx++;
			}
		}
	} 
}


/*************************************************************************************
 * 									MAIN LOOP										 *
 *************************************************************************************/

static void user_mainloop(void) {
	int i;
	while(1) {
		usleep(20000);
		// Process all devices
		for (i = 0; i < num_devices; i++) {
			hidraw_t *device = &hidraw_devices[i];
			// Set the request data to VERSION and send the request to the device
			device->h2c_frame.request.data = DATA_REQ_REPORT;
			write_to_device(device);
			// Check whether there is information available from the device
			read_and_parse(device);
		}
	}
	exit(0);
}
