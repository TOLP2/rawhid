#include <Encoder.h>
#include <HID-Project.h>
#include <HID-Settings.h>
#include <avdweb_Switch.h>

/*
 * When DEBUG is defined, all data is mirrored to Serial
 */
// #define DEBUG

/**
 * Version string which is shown while starting
 */
#define VERSION_STRING "hidraw v1.0 Fighting Furiosa"

/*
 * Definitions for the encoders. On the Arduino these pins must support interupts, on the Leonardo (Pro Micro) these
 * pins are 0, 1, 2, and 3. Using any of these two pins will enable fast mode on the Encoder Library.
 * 
 * NOTE: when using an older AtMega328 based boards (such as the Uno, Nano and clones), only pins 2 and 3 are supported.
 */ 
#define NUM_ENCODERS 3
const uint8_t channel_A[NUM_ENCODERS] = { 2, 0, 1};
const uint8_t channel_B[NUM_ENCODERS] = { 3, 4, 5};
Encoder *encoders[NUM_ENCODERS];


/*
 * Pins on which all switches are located. Although the numbering looks awkward, it corresponds with a counter clockwise numbering
 * on the Arduino Pro Micro, with four pins on each side. The numbering of the pins on the board is not sequential.
 */
#define NUM_DIGITAL_IN 4
const uint8_t digital_in_pins[NUM_DIGITAL_IN] = {6, 7, 8, 9};

/*
 * Pins on which all switches are located. Although the numbering looks awkward, it corresponds with a counter clockwise numbering
 * on the Arduino Pro Micro, with four pins on each side. The numbering of the pins on the board is not sequential.
 */
#define NUM_SWITCHES 3
const uint8_t switch_pins[NUM_SWITCHES] = {10, 16, 14};
uint32_t button_state = 0;
Switch *switches[NUM_SWITCHES];

// Buffer to hold RawHID data.
// If host tries to send more data than this, it will respond with an error.
// If the data is not read until the host sends the next data it will also 
// respond with an error and the data will be lost.
uint8_t xmitbuf[64];

/* Signal request of data  */
#define PKT_REQUESTDATA 0x01
#pragma pack(push,1)
enum dataRequested {DATA_REQ_NONE, DATA_REQ_VERSION, DATA_REQ_CAPABILITIES, DATA_REQ_REPORT};
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 3
  uint8_t id;       // id=PKT_REQUESTDATA
  dataRequested request;
} sPkt_RequestData;
#pragma pack(pop)

/* Signal response of the version */
#define PKT_VERSION 0x02
#pragma pack(push,1)
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 2
  uint8_t id;       // id=PKT_VERSION
  char string[sizeof(VERSION_STRING)];
} sPkt_Version;
#pragma pack(pop)

/* Signal response of the capabilities */
#define PKT_CAPABILITIES 0x03
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 2
  uint8_t id;         // id=PKT_CAPABILITIES
  uint8_t encoders;   // Number of encoders
  uint8_t digital_in; // Number of digital inputs
  uint8_t buttons;    // Number of buttons (digital inputs with debounce and click detection)
} sPkt_Capabilities;
#pragma pack(pop)

#define PKT_ENCODER_COUNTS 0x60
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 14 bytes
  uint8_t id;         // id=PKT_IMPERATOR_ENCODERS
  int32_t encoder[NUM_ENCODERS];
} sPkt_Imperator_encoder_v1;
#pragma pack(pop)


#define PKT_DIGITAL_IN 0x70
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 22 bytes
  uint8_t id;         // id=PKT_DIGITAL_IN_STATE
  uint32_t active;
} sPkt_RawHid_digital_in_v1;
#pragma pack(pop)

#define PKT_BUTTON_STATES 0x80
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 22 bytes
  uint8_t id;         // id=PKT_IMPERATOR_ENCODERS
  int32_t pushed;        
  int32_t released;         
  int32_t longPressed;     
  int32_t singleClicked;       
  int32_t doubleClicked;
} sPkt_Imperator_buttons_v1;
#pragma pack(pop)

/* Signal end of command packets. Note that no filler bytes are needed  */
#define PKT_ENDOFCOMMAND 0xff
#pragma pack(push,1)
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 2
  uint8_t id;       // id=PKT_ENDOFCOMMAND
} sPkt_Endofcommand;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
  sPkt_Imperator_encoder_v1 encoders;
  sPkt_RawHid_digital_in_v1 digital_in;
  sPkt_Imperator_buttons_v1 buttons;
  sPkt_Endofcommand eoc;
} sPkt_Report;
#pragma pack(pop)

#pragma pack(push,1)
union StateResponse_t {
  sPkt_Report report;
  byte RawHidPackage[64];
};
StateResponse_t stateResponse;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
  sPkt_Version version;
  sPkt_Endofcommand eoc;
} sPkt_VersionReport;
#pragma pack(pop)

#pragma pack(push,1)
union VersionResponse_t {
  sPkt_VersionReport report;
  byte RawHidPackage[64];
};
VersionResponse_t versionResponse;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
  sPkt_Capabilities capabilities;
  sPkt_Endofcommand eoc;
} sPkt_CapabilitiesReport;
#pragma pack(pop)

#pragma pack(push,1)
union CapabilitiesResponse_t {
  sPkt_CapabilitiesReport report;
  byte RawHidPackage[64];
};
CapabilitiesResponse_t capabilitiesResponse;
#pragma pack(pop)

void setup() {
  // put your setup code here, to run once:

  // Prepare the report-back message for the version
  versionResponse.report.version.id = PKT_VERSION;
  versionResponse.report.version.len=sizeof(sPkt_Version);
  strncpy(versionResponse.report.version.string, VERSION_STRING, sizeof(versionResponse.report.version.string));

  // Prepare the report-back message for the response
  capabilitiesResponse.report.capabilities.id = PKT_CAPABILITIES;
  capabilitiesResponse.report.capabilities.len = sizeof(sPkt_Capabilities);
  capabilitiesResponse.report.capabilities.encoders = NUM_ENCODERS;
  capabilitiesResponse.report.capabilities.digital_in = NUM_DIGITAL_IN;
  capabilitiesResponse.report.capabilities.buttons = NUM_SWITCHES;
  
  // Prepare the report-back message
  stateResponse.report.encoders.id=PKT_ENCODER_COUNTS;
  stateResponse.report.encoders.len=sizeof(sPkt_Imperator_encoder_v1);
  stateResponse.report.digital_in.id=PKT_DIGITAL_IN;
  stateResponse.report.digital_in.len=sizeof(sPkt_RawHid_digital_in_v1);
  stateResponse.report.buttons.id=PKT_BUTTON_STATES;
  stateResponse.report.buttons.len=sizeof(sPkt_Imperator_buttons_v1);
  stateResponse.report.eoc.id=PKT_ENDOFCOMMAND;
  stateResponse.report.eoc.len=sizeof(sPkt_Endofcommand);

  // Setup the encoders
  for (uint8_t i = 0; i < NUM_ENCODERS; i++) {
    encoders[i] = new Encoder(channel_A[i], channel_B[i]);
  }

  // Setup the digital, inputs
  for (uint8_t i = 0; i < NUM_DIGITAL_IN; i++) {
    pinMode(digital_in_pins[i], INPUT_PULLUP); 
  }

  // Setup the switches, including the callbacks 
  for (uint32_t i = 0; i < NUM_SWITCHES; i++) {
    switches[i] = new Switch(switch_pins[i]);
    switches[i]->setPushedCallback(&switchPushed, (void*) i);
    switches[i]->setReleasedCallback(&switchReleased, (void*) i);
    switches[i]->setLongPressCallback(&switchLongPressed, (void*) i);
    switches[i]->setDoubleClickCallback(&switchDoubleClicked, (void*) i);
    switches[i]->setSingleClickCallback(&switchSingleClicked, (void*) i);
  }

#ifdef DEBUG
  Serial.begin(115200);
#endif

  RawHID.begin(xmitbuf, sizeof(xmitbuf));
}

void loop() {
  // Parameter to store which data has been received
  dataRequested requested = DATA_REQ_NONE;

  // Check whether data is available and parse the commands from the data
  if (RawHID.available()) {
    uint8_t idx = 0;
    uint8_t *data;
    while (idx < sizeof(xmitbuf)) {
      data = &xmitbuf[idx];
      /* Determine packet type and process the packet */
      switch (data[1]) {
        case PKT_REQUESTDATA:
          requested = (((sPkt_RequestData *)data)->request);
          break;
        // To be extended when there are more commands in the future (such as set output)
      }
      /* go to next packet (or next byte if there was none) */
      if (data[0])
        idx += data[0];
      else
        idx++;
    }
    // Mark all data as 'read'
    RawHID.enable();
  }

  #ifdef DEBUG
  if (Serial.available()) {
    char serialData = Serial.read();
    switch (serialData) {
      case '1':
        reportVersion = true;
        break;
      case '2':
        reportState = true;
        break;
    }
  }
  #endif

  // Send the requested data to the host
  switch (requested) {
    case DATA_REQ_VERSION:
      RawHID.write(versionResponse.RawHidPackage, sizeof(versionResponse.RawHidPackage));
      #ifdef DEBUG
        for(uint8_t i=0; i<sizeof(versionResponse.RawHidPackage); i++){
          printHex(versionResponse.RawHidPackage[i]);
          Serial.println();
        }
      #endif
      break;
    case DATA_REQ_CAPABILITIES:
      RawHID.write(capabilitiesResponse.RawHidPackage, sizeof(capabilitiesResponse.RawHidPackage));
      #ifdef DEBUG
        for(uint8_t i=0; i<sizeof(capabilitiesResponse.RawHidPackage); i++){
          printHex(capabilitiesResponse.RawHidPackage[i]);
          Serial.println();
        }
      #endif
      break;
    case DATA_REQ_REPORT:
      // Read elements which do not automatically update the report
      reportEncoders();
      reportDigitalIn();
      // Send the report
      RawHID.write(stateResponse.RawHidPackage, sizeof(stateResponse.RawHidPackage));
      #ifdef DEBUG
        for(uint8_t i=0; i<sizeof(stateResponse.RawHidPackage); i++){
          printHex(stateResponse.RawHidPackage[i]);
          Serial.println();
        }
      #endif
      // Reset the report for the switches
      stateResponse.report.buttons.pushed = 0;
      stateResponse.report.buttons.released = 0;
      stateResponse.report.buttons.longPressed = 0;
      stateResponse.report.buttons.doubleClicked = 0;
      stateResponse.report.buttons.singleClicked = 0;
    default:
      break;
  }

  // Poll the switches, required to get the state
  for (uint8_t i = 0; i < NUM_SWITCHES; i++) {
    switches[i]->poll();
  }
  
}

/**
 * Callback functions which logs which button has been pressed
 */
void switchPushed(void* i) {
  stateResponse.report.buttons.pushed |= (1 << (uint32_t) i);
  #ifdef DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" pressed.");
  #endif
}
void switchReleased(void* i) {
  stateResponse.report.buttons.released |= (1 << (uint32_t) i);
  #ifdef DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" released.");
  #endif
}
void switchLongPressed(void* i) {
  stateResponse.report.buttons.longPressed |= (1 << (uint32_t) i);
  #ifdef DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" long pressed.");
  #endif
}
void switchDoubleClicked(void* i) {
  stateResponse.report.buttons.doubleClicked |= (1 << (uint32_t) i);
  #ifdef DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" double clicked.");
  #endif
}
void switchSingleClicked(void* i) {
  stateResponse.report.buttons.singleClicked |= (1 << (uint32_t) i);
  #ifdef DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" single clicked.");
  #endif
}

/**
 * Report the values on the encoder back to the 
 */
void reportEncoders() {
  for (uint8_t i = 0; i < NUM_ENCODERS; i++) {
    stateResponse.report.encoders.encoder[i] = encoders[i]->read();
    #ifdef DEBUG
      Serial.print("Encoder");  // print as an ASCII-encoded octal
      Serial.print(i, DEC);  // print as an ASCII-encoded octal
      Serial.print(":\t");  // print as an ASCII-encoded octal
      Serial.println(encoders[i]->read());  // print as an ASCII-encoded binary
    #endif
  }
}

void reportDigitalIn() {
  stateResponse.report.digital_in.active = 0;
  for (uint8_t i = 0; i < NUM_DIGITAL_IN; i++) {
    if (digitalRead(digital_in_pins[i]) == LOW) {
      stateResponse.report.digital_in.active |= (1 << i);
    }
  }
  #ifdef DEBUG
    Serial.print("Digital in: ");  // print as an ASCII-encoded octal
    Serial.println(stateResponse.report.digital_in.active, BIN);  // print as an ASCII-encoded binary
  #endif
}

#ifdef DEBUG
void printHex(char c) {
  char out[2];
  sprintf(out, "%02X", c);
  Serial.print(out);
}
#endif
