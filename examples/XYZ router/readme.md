# XYZ router
This folder contains the setup for the pendant of my XYZ-router. This is a simple pendant, consisting of:
- encoder, 100 ppr;
- hat-switch for selecting axis (4 positions);
- hat-switch for step-size (3 positions);
- start and pauze buttons;
- machine-stop.

This is a retrofit of an existing pendant, which is reluctantly connecting with a LPT-expansion card in the PC. 

## Board
The board that will be used will be a Arduino Pro-Micro clone which I have laying around.

![Pro micro pinout](https://cdn.sparkfun.com/assets/9/c/3/c/4/523a1765757b7f5c6e8b4567.png)

## Pins
The following pins will be used.

| Component      | Pins             | hidraw                             |
| -------------- | ---------------- | ---------------------------------- |
| 0, 1           | Handwheel A+, B+ | _encoder-0-count_                  |
| 6, 7, 8, 9     | Axis selection   | _digital-00-in_ to _digital-03-in_ | 
| 10, 16, 14     | Step selection   | _digital-04-in_ to _digital-06-in_ | 
| 20             | Start button     | _digital-07-in_                    | 
| 21             | Start button     | _digital-08-in_                    | 
| 2              | Machine stop     | _digital-09-in_                    | 

As there are abundant digital pins available for the number of inputs, no resistor ladders and analog inputs are used.


 
