/*
Copyright (c) 2021 Peter van Tol

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include "Encoder.h"
#include "HID-Project.h"
#include "HID-Settings.h"
#include "avdweb_Switch.h"

#include "config.h"
#include "communication/responses.h"

extern const uint8_t channel_A[HIDRAW_DEVICE_NUM_ENCODERS];
extern const uint8_t channel_B[HIDRAW_DEVICE_NUM_ENCODERS];
extern const uint8_t digital_in_pins[HIDRAW_DEVICE_NUM_DIGITAL_IN];

class HidRawDevice {
	public:
		void begin();
		void poll();
	
  private:
    //uint8_t xmitbuf[SERIAL_BUFFER_SIZE];
    dataRequested _requested;
    
    // Functions for reading and writing to the USB
    void _read();
    void _write();
    void _writeVersion();
    void _writeCapabilities();
    void _writeState();
    
    // Standardized reports
    VersionResponse_t versionResponse;
    CapabilitiesResponse_t capabilitiesResponse;
    StateResponse_t stateResponse;

#if HIDRAW_DEVICE_NUM_ENCODERS
    Encoder *_encoders[HIDRAW_DEVICE_NUM_ENCODERS];
    void reportEncoders();
#endif

#if HIDRAW_DEVICE_NUM_DIGITAL_IN
    void reportDigitalIn();
#endif

    /**
     * Callback functions which logs which button has been pressed
     */
#if HIDRAW_DEVICE_NUM_BUTTONS
    void switchPushed(void* i);
    void switchReleased(void* i);
    void switchLongPressed(void* i);
    void switchDoubleClicked(void* i);
    void switchSingleClicked(void* i);
    void resetButtonStates();
#endif
};
