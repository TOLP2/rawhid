/*
EXAMPLE: PENDANT

| Pins           | Type          | Component        | hidraw                             |
| -------------- | ------------- | ---------------- | ---------------------------------- |
| 0, 1           | Encoder       | Handwheel A+, B+ | _encoder-0-count_                  |
| 10, 16, 14, 15 | Digital in    | Axis selection   | _digital-00-in_ to _digital-03-in_ | 
| 7, 8, 9        | Digital in    | Step selection   | _digital-04-in_ to _digital-06-in_ | 
| 4              | Digital in    | Start button     | _digital-07-in_                    | 
| 5              | Digital in    | Pause button     | _digital-08-in_                    | 
| 6              | Digital in    | Machine stop     | _digital-09-in_                    | 


Copyright (c) 2021 Peter van Tol

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#pragma once

// Define the number of pins in use
#define HIDRAW_DEVICE_DESCRIPTION    "Pendant"
#define HIDRAW_DEVICE_SERIAL_NUMBER  0x01
#define HIDRAW_DEVICE_NUM_ENCODERS   1
#define HIDRAW_DEVICE_NUM_DIGITAL_IN 10
#define HIDRAW_DEVICE_NUM_BUTTONS    0

// Running in debug mode?
//#define HIDRAW_DEVICE_DEBUG

// Pin definitions
const uint8_t channel_A[HIDRAW_DEVICE_NUM_ENCODERS] = { 0 };
const uint8_t channel_B[HIDRAW_DEVICE_NUM_ENCODERS] = { 1 };
const uint8_t digital_in_pins[HIDRAW_DEVICE_NUM_DIGITAL_IN] = { 10, 16, 14, 15, 7, 8, 9, 4, 5, 6 };
