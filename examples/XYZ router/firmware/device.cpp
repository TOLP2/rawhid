/*
Copyright (c) 2021 Peter van Tol

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "device.h"

uint8_t xmitbuf[64];


#ifdef HIDRAW_DEVICE_DEBUG
void printHex(char c) {
  char out[5];
  sprintf(out, "%.2X ", c);
  Serial.print(out);
}
#endif

void HidRawDevice::begin() {
  
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.begin(115200);
  #endif
	
	// Prepare the report-back message for the version
	this->versionResponse.report.version.id = PKT_VERSION;
	this->versionResponse.report.version.len=sizeof(sPkt_Version);
	strncpy(this->versionResponse.report.version.description, HIDRAW_DEVICE_DESCRIPTION, sizeof(this->versionResponse.report.version.description));
  this->versionResponse.report.eoc.id=PKT_ENDOFCOMMAND;
  this->versionResponse.report.eoc.len=sizeof(sPkt_Endofcommand);

	// Prepare the report-back message for the response
	this->capabilitiesResponse.report.capabilities.id = PKT_CAPABILITIES;
	this->capabilitiesResponse.report.capabilities.len = sizeof(sPkt_Capabilities);
	this->capabilitiesResponse.report.capabilities.encoders = HIDRAW_DEVICE_NUM_ENCODERS;
	this->capabilitiesResponse.report.capabilities.digital_in = HIDRAW_DEVICE_NUM_DIGITAL_IN;
	this->capabilitiesResponse.report.capabilities.buttons = HIDRAW_DEVICE_NUM_BUTTONS;
  this->capabilitiesResponse.report.eoc.id=PKT_ENDOFCOMMAND;
  this->capabilitiesResponse.report.eoc.len=sizeof(sPkt_Endofcommand);

  // Prepare the report-back message
  #if HIDRAW_DEVICE_NUM_ENCODERS
  this->stateResponse.report.encoders.id=PKT_ENCODER_COUNTS;
  this->stateResponse.report.encoders.len=sizeof(sPkt_Imperator_encoder_v1);
  #endif
  #if HIDRAW_DEVICE_NUM_DIGITAL_IN
  this->stateResponse.report.digital_in.id=PKT_DIGITAL_IN;
  this->stateResponse.report.digital_in.len=sizeof(sPkt_RawHid_digital_in_v1);
  #endif
  #if HIDRAW_DEVICE_NUM_BUTTONS
  this->stateResponse.report.buttons.id=PKT_BUTTON_STATES;
  this->stateResponse.report.buttons.len=sizeof(sPkt_Imperator_buttons_v1);
  #endif
  this->stateResponse.report.eoc.id=PKT_ENDOFCOMMAND;
  this->stateResponse.report.eoc.len=sizeof(sPkt_Endofcommand);

#if HIDRAW_DEVICE_NUM_ENCODERS
  // Setup the encoders
  for (uint8_t i = 0; i < HIDRAW_DEVICE_NUM_ENCODERS; i++) {
    this->_encoders[i] = new Encoder(channel_A[i], channel_B[i]);
  }
#endif

#if HIDRAW_DEVICE_NUM_DIGITAL_IN
  // Setup the digital, inputs
  for (uint8_t i = 0; i < HIDRAW_DEVICE_NUM_DIGITAL_IN; i++) {
    pinMode(digital_in_pins[i], INPUT_PULLUP); 
  }
#endif

  // Start communicating over USB
  RawHID.begin(xmitbuf, sizeof(xmitbuf));
}


void HidRawDevice::poll() {
	this->_read();
	this->_write();	
	
	// Poll the switches, required to get the state
	for (uint8_t i = 0; i < HIDRAW_DEVICE_NUM_BUTTONS; i++) {
		//switches[i]->poll();
	}
}

void HidRawDevice::_read() {
	if (RawHID.available()) {
		uint8_t idx = 0;
		uint8_t *data;
		while (idx < sizeof(xmitbuf)) {
			data = &xmitbuf[idx];
			/* Determine packet type and process the packet */
			switch (data[1]) {
				case PKT_REQUESTDATA:
					this->_requested = static_cast<dataRequested> (((sPkt_RequestData *)data)->request);
					break;
					// To be extended when there are more commands in the future (such as set output)
			}
			/* go to next packet (or next byte if there was none) */
			if (data[0])
				idx += data[0];
			else
				idx++;
			}
		// Mark all data as 'read'
		RawHID.enable();
	}
	#ifdef HIDRAW_DEVICE_DEBUG
	if (Serial.available()) {
		char serialData = Serial.read();
		this->_requested = static_cast<dataRequested> (serialData - '0');
	}
	#endif
}


void HidRawDevice::_write() {
	switch (this->_requested) {
		case DATA_REQ_VERSION:
			this->_writeVersion();
			break;
		case DATA_REQ_CAPABILITIES: 
			this->_writeCapabilities();
			break;
    case DATA_REQ_REPORT: 
      this->_writeState();
      break;
	}
}

void HidRawDevice::_writeVersion() {
	RawHID.write(this->versionResponse.RawHidPackage, sizeof(this->versionResponse.RawHidPackage));
	this->_requested = DATA_REQ_NONE;
	#ifdef HIDRAW_DEVICE_DEBUG
	for(uint8_t i=0; i<sizeof(this->versionResponse.RawHidPackage); i++){
		printHex(this->versionResponse.RawHidPackage[i]);
	}
  Serial.println();
	#endif
};

void HidRawDevice::_writeCapabilities() {
	RawHID.write(this->capabilitiesResponse.RawHidPackage, sizeof(this->capabilitiesResponse.RawHidPackage));
	this->_requested = DATA_REQ_NONE;
	#ifdef HIDRAW_DEVICE_DEBUG
	for(uint8_t i=0; i<sizeof(this->capabilitiesResponse.RawHidPackage); i++){
		printHex(this->capabilitiesResponse.RawHidPackage[i]);
	}
  Serial.println();
	#endif
};

void HidRawDevice::_writeState() {
  // Read elements which do not automatically update the report
  #if HIDRAW_DEVICE_NUM_ENCODERS
  reportEncoders();
  #endif
  #if HIDRAW_DEVICE_NUM_DIGITAL_IN
  reportDigitalIn();
  #endif
  // Send the report
  RawHID.write(this->stateResponse.RawHidPackage, sizeof(this->stateResponse.RawHidPackage));
  #ifdef HIDRAW_DEVICE_DEBUG
  for(uint8_t i=0; i<sizeof(this->stateResponse.RawHidPackage); i++){
    printHex(this->stateResponse.RawHidPackage[i]);
  }
  Serial.println();
  #endif
  #if HIDRAW_DEVICE_NUM_BUTTONS
  // Reset the report for the switches
  this->resetButtonStates()
  #endif
  this->_requested = DATA_REQ_NONE;
};

#if HIDRAW_DEVICE_NUM_ENCODERS
/**
 * Report the values on the encoder back to the 
 */
void HidRawDevice::reportEncoders() {
  for (uint8_t i = 0; i < HIDRAW_DEVICE_NUM_ENCODERS; i++) {
    this->stateResponse.report.encoders.encoder[i] = this->_encoders[i]->read();
    #ifdef HIDRAW_DEVICE_DEBUG
      Serial.print("Encoder");  // print as an ASCII-encoded octal
      Serial.print(i, DEC);  // print as an ASCII-encoded octal
      Serial.print(":\t");  // print as an ASCII-encoded octal
      Serial.println(this->_encoders[i]->read());  // print as an ASCII-encoded binary
    #endif
  }
}
#endif

#if HIDRAW_DEVICE_NUM_DIGITAL_IN
void HidRawDevice::reportDigitalIn() {
  this->stateResponse.report.digital_in.active = 0;
  for (uint8_t i = 0; i < HIDRAW_DEVICE_NUM_DIGITAL_IN; i++) {
    if (digitalRead(digital_in_pins[i]) == LOW) {
      this->stateResponse.report.digital_in.active |= (1 << i);
    }
  }
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.print("Digital in: ");  // print as an ASCII-encoded octal
    Serial.println(this->stateResponse.report.digital_in.active, BIN);  // print as an ASCII-encoded binary
  #endif
}
#endif

#if HIDRAW_DEVICE_NUM_BUTTONS
/**
 * Callback functions which logs which button has been pressed
 */
void HidRawDevice::switchPushed(void* i) {
  this->stateResponse.report.buttons.pushed |= (1 << (uint32_t) i);
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" pressed.");
  #endif
}
void HidRawDevice::switchReleased(void* i) {
  this->stateResponse.report.buttons.released |= (1 << (uint32_t) i);
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" released.");
  #endif
}
void HidRawDevice::switchLongPressed(void* i) {
  this->stateResponse.report.buttons.longPressed |= (1 << (uint32_t) i);
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" long pressed.");
  #endif
}
void HidRawDevice::switchDoubleClicked(void* i) {
  this->stateResponse.report.buttons.doubleClicked |= (1 << (uint32_t) i);
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" double clicked.");
  #endif
}
void HidRawDevice::switchSingleClicked(void* i) {
  this->stateResponse.report.buttons.singleClicked |= (1 << (uint32_t) i);
  #ifdef HIDRAW_DEVICE_DEBUG
    Serial.print("Button ");
    Serial.print((uint32_t) i, DEC);
    Serial.println(" single clicked.");
  #endif
}
void HidRawDevice::resetButtonStates(){
  this->stateResponse.report.buttons.pushed = 0;
  this->stateResponse.report.buttons.released = 0;
  this->stateResponse.report.buttons.longPressed = 0;
  this->stateResponse.report.buttons.doubleClicked = 0;
  this->stateResponse.report.buttons.singleClicked = 0;
}
#endif
