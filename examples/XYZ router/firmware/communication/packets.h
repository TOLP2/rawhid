/*
Copyright (c) 2021 Peter van Tol

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
// Include guard
#pragma once

/* Signal request of data  */
#define PKT_REQUESTDATA 0x01
#pragma pack(push,1)
enum dataRequested {DATA_REQ_NONE, DATA_REQ_VERSION, DATA_REQ_CAPABILITIES, DATA_REQ_REPORT};
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 3
  uint8_t id;       // id=PKT_REQUESTDATA
  dataRequested request;
} sPkt_RequestData;
#pragma pack(pop)

/* Signal response of the version */
#define PKT_VERSION 0x02
#pragma pack(push,1)
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 2
  uint8_t id;       // id=PKT_VERSION
  char description[sizeof(HIDRAW_DEVICE_DESCRIPTION)];
} sPkt_Version;
#pragma pack(pop)

/* Signal response of the capabilities */
#define PKT_CAPABILITIES 0x03
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 2
  uint8_t id;         // id=PKT_CAPABILITIES
  uint8_t encoders;   // Number of encoders
  uint8_t digital_in; // Number of digital inputs
  uint8_t buttons;    // Number of buttons (digital inputs with debounce and click detection)
} sPkt_Capabilities;
#pragma pack(pop)

#ifdef HIDRAW_DEVICE_NUM_ENCODERS
#define PKT_ENCODER_COUNTS 0x60
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 14 bytes
  uint8_t id;         // id=PKT_IMPERATOR_ENCODERS
  int32_t encoder[HIDRAW_DEVICE_NUM_ENCODERS];
} sPkt_Imperator_encoder_v1;
#pragma pack(pop)
#endif

#ifdef HIDRAW_DEVICE_NUM_DIGITAL_IN
#define PKT_DIGITAL_IN 0x70
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 22 bytes
  uint8_t id;         // id=PKT_DIGITAL_IN_STATE
  uint32_t active;
} sPkt_RawHid_digital_in_v1;
#pragma pack(pop)
#endif

#ifdef HIDRAW_DEVICE_NUM_BUTTONS
#define PKT_BUTTON_STATES 0x80
#pragma pack(push,1)
typedef struct {
  uint8_t len;        // packet length in bytes, including this one = 22 bytes
  uint8_t id;         // id=PKT_IMPERATOR_ENCODERS
  int32_t pushed;        
  int32_t released;         
  int32_t longPressed;     
  int32_t singleClicked;       
  int32_t doubleClicked;
} sPkt_Imperator_buttons_v1;
#pragma pack(pop)
#endif

/* Signal end of command packets. Note that no filler bytes are needed  */
#define PKT_ENDOFCOMMAND 0xff
#pragma pack(push,1)
typedef struct {
  uint8_t len;      // packet length in bytes, including this one = 2
  uint8_t id;       // id=PKT_ENDOFCOMMAND
} sPkt_Endofcommand;
#pragma pack(pop)
