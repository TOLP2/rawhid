#include "config.h"
#include "device.h"

HidRawDevice device;

void setup() {
  device.begin();
}

void loop() {
  device.poll();
}
