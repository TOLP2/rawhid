# Rawhid

A LinuxCNC hal-component for talking to a home-made Arduino based Human Input Device, such as a pendant.

## Introduction

**rawhid** is an LinuxCNC userspace component to provide access to own created USB HID RAW devices. One can easily create their own HID device using a cheap Arduino and some buttons or encoders to suit their needs, with minimal coding required.

Why another HID interface component when LinuxCNC already has **hidcomp**, **hal_input** and **hal_joystick**?
- **rawhid** was written to support my home made USB pendant for my gantry router and the control panel for my Emco 5 CNC Retrofit. Both applications have a different number of inputs, while I still want to only maintain one codebase.
- **rawhid** supports digital inputs, buttons with built-in debounce, quadrature encoders, digital output (both direct and with a 74HC595 shift/latch register), analog in and analog out (PWM). Configuration is done using a simple Arduino sketch, defining the number and type of inputs and their pins. Future expansions can be easily made by creating new data-packages and code in the component to create the required pins.
- **rawhid** exports only pins which are available on the device.
- **rawhid** makes it easy to find and configure HID devices. Only the VID and PID of the device are required. NOTE: at this moment multiple devices with the same VID and PID are not supported yet. In the future the Arduino sketch will be expanded with a Serial Number, to prevent awkward changing the VID and PID for the board.
- **rawhid** does not rely on any external library.

**rawhid** is based on the assumption that you create your own pendant based on an Arduino or compatible platform. To connect existing products, such as joysticks, gamepads, multimedia controllers (like the Shuttle Pro), please consider to use [hidcomp](http://hidcomp.sourceforge.net/).

## Supported boards
All boards where the communication is done by the AtMega32U4 can be used. These include the official Leonardo, Micro and their clones such as the Pro Micro. Other boards which support the Arduino IDE are also supported, an example the STM32F103C8T6 'Blue pill'.

Boards where USB-configuration is done by a CH320 chipset or an external AtMega16U2 cannot be used, because these devices will always be listed as Serial (ttyS##) and cannot be changed to a HID device.

There are also AtTiny84 based boards available, which work with V-USB. At this moment it is not clear whether these boards can be used as a HID device.

## Supported inputs and outputs
The HID RAW communication is based on a regular communication where 64-byte data commands are send from the host to the device and vice-versa. Of these 64 bytes, the last two bytes are used to signal the end of the command, leaving effectively 62 bytes. This packet size determines the amount of inputs and outputs which can be added to the Arduino 

| Data type         | Common usage                          | Direction | Max. number  | Max. datasize*  | Remarks |
| ----------------- | ------------------------------------- | --------- | ------------ | --------------- | ------- |
| encoder           | Quadrature encoder, handwheel         | in        | 4            | 2 + 4 per input |         |
| digital-in        | Switches, buttons                     | in        | 32           | 6               |         |
| analog-in         | Hat-switch (R-ladder), potentiometer  | in        | 7            | 16              |         |
| button            | Buttons (with debounce)               | in        | 32           | 22              |         |
| digital-out       | LED                                   | out       | 32           | 6               |         |
| analog-out        | PWM                                   | out       | 32           | 6               |         |

The encoders require a pin with external interrupt on at least one of the inputs. On the Leonardo, these pins are 0, 1, 2, 3, 7 (last pin not exported on the Pro Micro clones). Based on the number of external interrupts the maximum number of encoders has been chosen.

For the digital-in, digital-out and the buttons the maximum number has been chosen based on the data of an integer (32-bits). This a far larger number than the available pins on the Arduino. These can be extended by using shift-in (74HC165) or shift-out (74HC595) registers. The use of external registers and local pins is mutal exclusive.

_Analog-in is not yet implemented_. However it seems that the number of analog inputs is limited due to the package size. The Leonardo has 12 analog inputs, while only 7 can be used at this time.

_Analog-out is not yet implemented_.

## Installing rawhid userspace component

### Getting LinuxCNC development package
If you’re working with an installed version of LinuxCNC you will need to install the development packages. One method is to say following line in a terminal.
```bash
sudo apt-get install linuxcnc-dev
```


Another method is to use the Synaptic Package Manager from the main Ubuntu menu to install linuxcnc-dev.

### Installation
Download the file rawhid.c from this repository:
```bash
TODO
```

Then install it using the following command
```bash
halcompile --install rawhid.c
```

## Usage
**rawhid** is a user space component so it must be loaded into EMC/LinuxCNC using loadusr:
```
loadusr -W rawhid [count=N|names=name1[,name2...]] cfg="0x2341:0x8037"
```

**rawhid** can take a while to start, so it is recommended to use the -W option of loadusr to wait for the component to start. The component requires a configuration string, which consists of VID:PID combinations for each of the device. The number of VID:PID combinations must be equal to _count_ or the number of _names_, otherwise an error will be thrown while starting the component. The default numver of devices is 1.

### Exported Pins
When run, **rawhid** will export a large number of pins based on the reported capabilities of the device. These can then be used in the LinuxCNC HAL file to control parts of the LinuxCNC machine. The simplest way to list all of the pins is to add the `loadusr -W rawhid cfg="0x2341:0x8037"` line to an LinuxCNC .hal file, run LinuxCNC, then browse the pins using the configuration browser. This is the "Show Hal Configuration" menu option in the "Machine" menu in Axis.

## Permissions
When you insert a USB device, by default, the device is owned by `root`. A solution is to create a udev rules file. A file has to be created in `/etc/udev/rules.d` which tells udev (the dynamic device management system) what to do when the device is inserted. 

### Neat solution
The following line(s) are placed in a .rules file (for example 99-rawhid-permissions.rules) located under /etc/udev/rules.d (note: replace the VID and PID of your device):
```
# Arduino Leonardo
SYSFS{idVendor}=="2341", SYSFS{idProduct}=="8037", MODE="0660", GROUP="plugdev"
```

For this file to work, there must also be a group on the system called plugdev, and the user account that runs LinuxCNC must belong to this group. This can be done with the commands (note: replace `<login-name>` with your login-name on the system):
```bash
sudo addgroup plugdev
sudo addgroup <login-name> plugdev
```

### Alternative solution
The line below will set read/write permissions for all users for any hidraw device inserted to your system.
```
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0660", GROUP="plugdev"
```

### For a single test
For a single test, one can also set the permissions temporary:
```bash
sudo chmod 666 \dev\hidraw*
```

